
package database;


import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DatabaseManager {

    Connection con;
    Statement st;
    String tableName;

    public DatabaseManager(String host, String port, String db, String user, String pass)
    {
        
        if (host==null) host="localhost";
        if (port==null) port="3306";
        if (db==null) db="default";
        if (user==null) user="root";
        if (pass==null) pass="";
        String url ="jdbc:mysql://"+host+":"+port+"/"+db;

        try {
            con = DriverManager.getConnection( url, user, pass);
            st = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                    } catch (java.sql.SQLException e) {
            System.out.println("Connection couldn't be established to " + url);
        }
    }

    public void selectTable(String str){
        tableName=str;
    }

    public Connection getConnection() {
                return (con);
    }

    public List getColumnHeaders(){
        List list = new ArrayList();

String query = "SELECT * FROM "+ tableName;
        try {
            ResultSet rs = st.executeQuery(query);
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int numberOfColumns = rsMetaData.getColumnCount();
            for (int i = 1; i < numberOfColumns + 1; i++) {
            String columnName = rsMetaData.getColumnName(i);
            list.add(columnName);
    }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Getting column headers failed!");
        }
        
        return list;
    }

    public int getRowCount(){
        String query = "SELECT * FROM "+ tableName;
        int rowcount=0;
        try{
            ResultSet rs = st.executeQuery(query);
            while (rs.next())
{
   rowcount ++;
}         }
        catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Getting row count failed!");
            }
        return rowcount;
    }
            

    public String[] getTheRow(int columns,int row){
       
        String data[]=new String[columns];
      String query = "SELECT * FROM "+ tableName;
        try {
            ResultSet rs = st.executeQuery(query);
if (rs.absolute(row)){
            for (int i=0;i<columns;i++) data[i]=rs.getString(i+1);

           }
 } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Getting row data failed!");
            }
                return data;
    }

    public List getRows(int columns){
List list = new LinkedList();
        list = new ArrayList();
        String data[]=new String[columns];
      String query = "SELECT * FROM "+ tableName;
        try {
            ResultSet rs = st.executeQuery(query);
while (rs.next()){
            for (int i=0;i<columns;i++) data[i]=rs.getString(i+1);
list.add(data);
           }
 } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Getting row data failed!");
            }

      
                return list;
    }

    public String validateValue(String query) {
        String result="";
if (query!=null){
        result = query.replaceAll("'", "");
}
        return result;
    }

    public static void main(String args[]) {
        
        DatabaseManager dm= new DatabaseManager(null, null, "msgsys", null, null);
//        dm.updateCell("msg", 0, 3, "tester");
        
    }

    public void updateCell(int row,int column,String value) {
        value=validateValue(value);
        String query = "SELECT * FROM "+tableName;
        try {
            ResultSet rs = st.executeQuery(query);
if (rs.absolute(row)){
            rs.updateString(column,value);
            rs.updateRow(); 

           }
 } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Updating failed!");
            }
        
    }

    public void insertEmtyRow(int columnCount) {
         String query = "INSERT INTO "+tableName+" VALUES ('20'";
         for (int i=1;i<columnCount;i++){
             query+=",'1' ";
         }
         query+=")";
        try {
            System.out.println(query);
            st.execute(query);
            
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Create New Row failed!");

            
        }
    }

    public boolean deleteRow(int row) {
        
        String query = "SELECT * FROM "+tableName;
        try {
            ResultSet rs = st.executeQuery(query);
                      
if(rs.absolute(row)) rs.deleteRow();


            return true;
 } catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Deleting Row failed!");
            return false;
            }
    }

    
    public String getColumnType(int co) {
        String query = "SELECT * FROM "+ tableName;
        try {
            ResultSet rs = st.executeQuery(query);
            ResultSetMetaData rsMetaData = rs.getMetaData();
            return rsMetaData.getColumnTypeName(co);
            
            
    }
         catch (java.sql.SQLException e) {
            e.printStackTrace();
            System.out.println("Getting column headers failed!");
            return "";
        }
    }
}




