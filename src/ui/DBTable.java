/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import database.DatabaseManager;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;


/**
 *
 * @author Administrator
 */
public class DBTable extends JTable{


    //Global Variables
      DefaultTableModel tableModel;
      DatabaseManager dm;
      String db;
      int rowCount;
      int columnCount;
      int ro;
      int co;
      ListSelectionModel listSelectionModel;
      Set selectedRows = new TreeSet();
      JTable table=this;
      String old;
      
    public DBTable(String host,String username,String password,String database,String dbTableName){

      
      List list = new ArrayList();
      dm=new DatabaseManager(host,null,database,username,password);
      dm.selectTable(dbTableName);

      //Get Table headers and set table model
      list=dm.getColumnHeaders();
      String[] columnHeaders = (String[]) list.toArray(new String[]{});
      columnCount=columnHeaders.length;
      tableModel=new DefaultTableModel(columnHeaders,0);
      this.setModel(tableModel);
      

     // this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

      
     //set row selection model
      listSelectionModel = this.getSelectionModel();
      listSelectionModel.addListSelectionListener(new SharedListSelectionHandler());
      this.setSelectionModel(listSelectionModel);
      setForeground(Color.red);
      setBackground(new Color(255, 239, 185));
      setGridColor(Color.orange);
      setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      setRowHeight(35);


      //Get and insert rows
      rowCount=dm.getRowCount();
      for (int i=rowCount;i>0;i--){
      String row[]=dm.getTheRow(columnCount, i);
      tableModel.insertRow(0, row);
       }

      //Set custom Cell Properties
      for(int i = 0; i<tableModel.getColumnCount(); i++) {
        TableColumn column = getColumn(tableModel.getColumnName(i));
        column.setCellRenderer(new GenericRenderer());
        column.setCellEditor(new MyEditor(new JTextField()));
      }
       }

    public void saveTable() {
        for (int i=0;i<rowCount;i++)
            for (int j=0;j<columnCount;j++){
                String a=(String) this.getValueAt(i,j);
                dm.updateCell(i+1,j+1,a);}
                        
        }

    public boolean validateValue(){
        String a = (String) this.getValueAt(ro,co);
        int r=ro;
        int c=co;
        //if column is for integers
        if(dm.getColumnType(co+1).matches("(?i).*int.*")){
            //check if integer entered
            if(!Pattern.matches("^\\d*$", a)){
                JOptionPane.showMessageDialog(this.getParent(), "You entered non-integer value in an integer column!","Error",JOptionPane.ERROR_MESSAGE);
                this.setValueAt(old, r, c);
                this.requestFocus();
                this.changeSelection(r, c, false, false);
                                
            }
            

        }
       
        return false;
    }


    public void reTable(){

    }


    public void insertNewRow() {
String[] row={};
tableModel.addRow(row);
dm.insertEmtyRow(columnCount);
rowCount++;
}

    void deleteRows() {
        
        Object[] a = selectedRows.toArray();
        Integer[] c = new Integer[a.length];
     for(int i = 0; i < a.length; i++)
{
    c[i] = (Integer) a[i];
    if(dm.deleteRow(c[i]+1-i)){
        tableModel.removeRow(c[i]-i);
        
    }
}

    }

    


    class SharedListSelectionHandler implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            selectedRows.removeAll(selectedRows);
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            if (!lsm.isSelectionEmpty()) {
                     for (int i = lsm.getMinSelectionIndex(); i <= lsm.getMaxSelectionIndex(); i++) {
                    if (lsm.isSelectedIndex(i)) {
                        selectedRows.add(i);
                                            }
                }
            }
            
        }
    }

     private class GenericRenderer extends DefaultTableCellRenderer {
        @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
    boolean isSelected, boolean hasFocus, int row, int col) {
      if(hasFocus) {
          ro=row;co=col;
        table.getCellEditor( row, col ).getTableCellEditorComponent(table, new Object(), false, row, col );
        TableCellEditor ce =table.getCellEditor(row, col);
        if (ce != null) ce.stopCellEditing();
 }
      Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
      return(c);
    }
  }

     public class MyEditor extends DefaultCellEditor {

   public MyEditor(JTextField textField) {
      super(textField);
       JTextField txt;
            txt = (JTextField) super.getComponent();

      txt.addFocusListener(new FocusAdapter() {
            @Override
         public void focusLost(FocusEvent e) {
            stopCellEditing() ;

                validateValue();
                //System.out.println(dm.getColumnType(tableName,co+1));
         }
                @Override
            public void focusGained(FocusEvent e) {
                old=(String) table.getValueAt(ro, co);
                
                    }
      });
   }
 
}
}